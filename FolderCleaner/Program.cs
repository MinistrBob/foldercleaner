﻿using log4net;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace FolderCleaner
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();

            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);

            string dirpath = Properties.Settings.Default.DirPath;

            try
            {
                log.Info(String.Format("############################################################################"));
                log.Info(String.Format("Работаю в каталоге: {0}", dirpath));
                Console.WriteLine("Работаю в каталоге: {0}", dirpath);
                string[] dirs = Directory.GetDirectories(dirpath);
                //Console.WriteLine("Всего каталогов: {0}", dirs.Length);
                log.Info(String.Format("Всего каталогов: {0}", dirs.Length));

                foreach (string dir in dirs)
                {
                    WorkWithDir(dir, false);

                }
                log.Info(String.Format("############################################################################"));
            }
            catch (Exception e)
            {
                log.Info(String.Format("Ошибка: {0}", e.ToString()));
                Console.WriteLine("Ошибка: {0}", e.ToString());
                throw;
            }

        }

        static private void WorkWithDir(string path, bool issubfolder)
        {
            log.Info(String.Format("Обработка каталога: {0}", path));
            //Console.WriteLine("Обработка каталога: {0}", path);

            WorkWithFiles(path);

            try
            {
                string[] dirs = Directory.GetDirectories(path);
                log.Info(String.Format("Всего подкаталогов: {0}", dirs.Length));
                //Console.WriteLine("Всего подкаталогов: {0}", dirs.Length);
                foreach (string dir in dirs)
                {
                    WorkWithDir(dir, true);
                    log.Info(String.Format(@"-----------------------------------------"));
                    //Console.WriteLine(@"-----------------------------------------");

                }
            }
            catch (Exception e)
            {
                log.Info(String.Format("Ошибка: {0}", e.ToString()));
                Console.WriteLine("Ошибка: {0}", e.ToString());
            }

            if (issubfolder)
            {
                // Если папка стала пустой - удаляем её
                if (!Directory.EnumerateFileSystemEntries(path).Any())
                {
                    DateTime dirCreatedDate = Directory.GetCreationTime(path);
                    var deltatime = Math.Ceiling(Math.Abs((DateTime.Now - dirCreatedDate).TotalDays));
                    log.Info(String.Format("Время создания папки: {0}; Разница в днях {1}", dirCreatedDate, deltatime));
                        //Console.WriteLine("Время создания: {0}", fileCreatedDate);
                    if (deltatime > 14)
                        {
                            log.Info(String.Format("Удаление пустой папки: {0}", path));
                            Console.WriteLine("Удаление пустой папки: {0}", path);
                            
                            try
                            {
                                Directory.Delete(path);
                            }
                            catch (Exception e)
                            {
                                log.Info(String.Format("Ошибка удаления пустой папки {0}: {1}", path, e.ToString()));
                                Console.WriteLine("Ошибка удаления пустой папки {0}: {1}", path, e.ToString());
                            }
                        }
                }
            }
        }

        static private void WorkWithFiles(string path)
        {
            try
            {
                string[] files = Directory.GetFiles(path);
                log.Info(String.Format("Всего в папке {0} файлов", files.Length));
                //Console.WriteLine("Всего в папке {0} файлов", files.Length);
                foreach (string file in files)
                {
                    DateTime fileCreatedDate = File.GetLastWriteTime(file);
                    var deltatime = Math.Ceiling(Math.Abs((DateTime.Now - fileCreatedDate).TotalDays));
                    log.Info(String.Format("Время создания файла: {0}; Разница в днях {1}", fileCreatedDate, deltatime));
                    //Console.WriteLine("Время создания: {0}", fileCreatedDate);
                    if (deltatime > 14)
                    {
                        log.Info(String.Format("Удаление файла: {0}", file.ToString()));
                        //Console.WriteLine("Удаление файла: {0}", file.ToString());
                        if ((File.GetAttributes(file) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                            File.SetAttributes(file, FileAttributes.Normal);
                        try
                        {
                            File.Delete(file);
                        }
                        catch (Exception e)
                        {
                            log.Info(String.Format("Ошибка удаления файла {0}: {1}", file, e.ToString()));
                            Console.WriteLine("Ошибка удаления файла {0}: {1}", file, e.ToString());
                        }
                    }


                }


            }
            catch (Exception e)
            {
                log.Info(String.Format("Ошибка WorkWithFiles: {0}", e.ToString()));
                Console.WriteLine("Ошибка WorkWithFiles: {0}", e.ToString());
            }

        }

        // ---------------------------------------------------------------------------------------------------------------
        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            LogUnhandledException(e.ExceptionObject as Exception);
        }


        // ---------------------------------------------------------------------------------------------------------------

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            LogUnhandledException(e.Exception);
        }

        // ---------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// запись информации о необработанной ошибке в файл
        /// </summary>
        public static void LogUnhandledException(Exception exception)
        {
            string msg = string.Format(
                        @"Произошла непредвиденная ошибка и программа была закрыта.
                        Текст ошибки: {0}
                        Стек вызовов: {1}", exception.Message, exception.StackTrace);

            // Пишем файл с ошибкой и стеком в папку откуда запускается программа, если не получается - в папку %TEMP\egrp%
            try
            {
                File.WriteAllText(
                        Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "!!!FolderCleaner_exception.txt"),
                        DateTime.Now + "\r\n" + msg, System.Text.Encoding.GetEncoding(1251));
            }
            catch (Exception)
            {

                try
                {
                    File.WriteAllText(
                        Path.Combine(Path.GetTempPath(), "!!!FolderCleaner_exception.txt"),
                        DateTime.Now + "\r\n" + msg, System.Text.Encoding.GetEncoding(1251));
                }
                catch (Exception)
                {


                }
            }

            ILog log = LogManager.GetLogger(typeof(Program));
            log.Fatal(msg, exception);

            try
            {
                if (!EventLog.SourceExists("FolderCleaner.exe"))
                    EventLog.CreateEventSource("FolderCleaner.exe", "Application");
                EventLog el = new EventLog("Application");
                el.Source = "FolderCleaner.exe";
                el.WriteEntry(msg, EventLogEntryType.Error, 999, 0);
                el.Close();
            }
            catch
            {
            }
        }

    }
}
